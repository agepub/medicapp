# Medicap: The Life-Saving Smart Cap

![alt text](https://www.agevoluzione.com/wp-content/uploads/2023/04/Medicap_img.png)

## Introduction

Medicap is an innovative intelligent medical device designed to improve medication adherence for patients. Emerging within the digital and automated solutions in the healthcare field, Medicap integrates with existing medication containers equipped with screw caps, offering a range of features that simplify medication management and correct intake.

## Key Features
Smart and Integrable Cap: Medicap is an intelligent screw cap that can be easily adapted to existing medication containers, whether for pills, liquids, or powders. It is designed to replace the original cap, transforming the container into a monitoring and assistance device.
Reminder for Intake: With its built-in sensors, Medicap keeps track of the opening and closing times of the container and sends notifications to remind patients of proper medication intake, ensuring optimal adherence.
Residual Quantity Monitoring: Medicap constantly monitors the remaining quantity of medication in the container, providing timely alerts when the content is running low. This allows patients to plan medication refills appropriately.
Medication Expiry Control: Thanks to its intelligent capabilities, Medicap detects the medication's expiration date and alerts the patient when the medication is about to lose its effectiveness. This helps ensure that only valid and safe medications are used.
Temperature Monitoring: In case the container is stored in an unsuitable environment, Medicap detects and monitors the temperature, notifying the patient if the medication is at risk of being compromised.
Wireless BLE Connectivity: The information collected by Medicap is securely transferred to a mobile device using Bluetooth Low Energy (BLE). Through a dedicated app, patients can monitor their medication intake, view historical data, and receive useful information about their medication therapy.


## Research Findings
Medicap has been developed considering pharmaceutical standards for containers and caps used in the medical industry. Prior to the product realization, extensive studies and research were conducted to ensure full compatibility and compliance with safety and quality requirements. Please refer to the attached document for more details on the research findings regarding pharmaceutical container standards.

## Aesthetic Definition and Research
In addition to its advanced functionalities, Medicap has been carefully designed to offer an aesthetically pleasing experience while maintaining its practicality and functionality. Extensive research and design considerations have been undertaken to create a visually appealing and user-friendly product.

![alt text](https://www.agevoluzione.com/wp-content/uploads/2023/04/medicap_project_03.png)

## Example of a configuration request

```
{"pld":{"qcf":"dt"},"crc":1234}\n
{"pld":{"cf":{"dt":230110045633}},"crc":1234)}r
{"pld":{"qcf":”dep”},"crc":1234}\n
{"pld":{"cf":{“dep”:120}},"crc":1234}\n
```

[Research Results Studies Standard Pharmaceutical Bottles](https://www.agevoluzione.com/wp-content/uploads/2023/05/AN_Risultati-Ricerca-Studi-Standard-Flaconi-Farmaceutici.pdf)

[Aesthetic definition and research](https://www.agevoluzione.com/wp-content/uploads/2023/05/Definizione-estetica-e-ricerca-1.pdf)

![alt text](https://www.agevoluzione.com/wp-content/uploads/2023/04/progetto_fesr.jpg)

Note: The images used in this document are for illustrative purposes only and may not reflect the exact appearance of the final product.
